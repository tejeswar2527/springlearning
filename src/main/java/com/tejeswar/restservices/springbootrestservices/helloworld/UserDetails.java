package com.tejeswar.restservices.springbootrestservices.helloworld;

public class UserDetails {
	
	private String name;
	private String city;
	private int id;
	
	
	public UserDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserDetails(String name, String city, int id) {
		super();
		this.name = name;
		this.city = city;
		this.id = id;
	}
		// TODO Auto-generated constructor stub
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "UserDetails [name=" + name + ", city=" + city + ", id=" + id + "]";
	}
	

}
