package com.tejeswar.restservices.springbootrestservices.helloworld;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
	
	
	@GetMapping("/helloworld")
	public String helloWorld() {
		return "hello World";
	}
	@GetMapping("/helloworld2")
	public UserDetails helloWorldBean() {
		return new UserDetails("teja", "hyd", 1);
	}
	
	
	

}
