package com.tejeswar.restservices.springbootrestservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//
@SpringBootApplication
public class SpringbootTejeswarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootTejeswarApplication.class, args);
	}

}
